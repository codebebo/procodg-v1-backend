<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignProjectToUser extends Model
{
    protected $table = 'project_assign_users';
}
