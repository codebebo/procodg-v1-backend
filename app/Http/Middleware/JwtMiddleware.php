<?php
namespace App\Http\Middleware;

use Closure;
use Exception;
use App\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Request;
class JwtMiddleware {
    public function handle(Request $request, Closure $next, $guard = null) {
        $token = $request->bearerToken();
        if(!$token) {
            return response()->json([
                'error' => true,
                'token_expired' => true,
                'message'=>trans('auth.token')
            ], 401);
        }

        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch(ExpiredException $e) {
            return response()->json([
                'error' => true,
                'token_expired' => true,
                'message'=>trans('auth.token_expired')
            ], 400);
        } catch(Exception $e) {
            return response()->json([
                'error' =>true,
                //'token_expired' => true,
                'message'=>trans('auth.token_decode').' '.$token
            ], 400);
        }

        $user = User::find($credentials->sub);
        $request->auth = $user;

        return $next($request);
    }
}