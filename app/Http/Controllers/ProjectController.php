<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\AssignProjectToUser;
use DB, Validator, Auth;

class ProjectController extends Controller {

    public function __construct() {
        
    }

    public function index(Request $request) {        
        /*$projectObj = Project::select(DB::raw("MD5(id) as projectID, name, client_name, description, FROM_BASE64(project_detail) as project_detail, DATE_FORMAT(created_at, '%b %d, %Y') as created, created_at, DATE_FORMAT(updated_at, '%b %d, %Y') as updated, updated_at"));*/
        $projectObj = Project::select(DB::raw("MD5(id) as projectID, name, client_name, description, FROM_BASE64(project_detail) as project_detail, DATE_FORMAT(created_at, '%b %d, %Y') as created, created_at, DATE_FORMAT(updated_at, '%b %d, %Y') as updated, updated_at"), DB::raw("(SELECT SUM(project_status.tracking_hours) FROM project_status WHERE project_status.project_id = projects.id GROUP BY project_status.project_id) as hours"));
        if($request->auth->role!='admin'){
            $projectObjAssign = AssignProjectToUser::where('user_id','=',$request->auth->id)->pluck('project_id');
            $projectObj->whereIn('id',$projectObjAssign);
            $projectObj->orWhere('creator_id',$request->auth->id);
        }
        if($request->input('q')!==""){
            $projectObj->where('name','like', $request->input('q').'%');
            $projectObj->orWhere('client_name','like',$request->input('q').'%');
        }
        $projectObj->where('status','=',1);

        $projects = $projectObj->paginate(12);
        $data['error'] = false;
		$data['message'] = trans('message.MESSAGE_SUCCESS');
		$data['data'] = $projects->items();
        $data['paging']['total'] = $projects->total();
        $data['paging']['prev'] = ($projects->currentPage()>1)?($projects->currentPage() - 1):null;
        $data['paging']['current'] = $projects->currentPage();
        $data['paging']['next'] = ($projects->currentPage()<$projects->lastPage())?($projects->currentPage() + 1):null;
        $data['paging']['per_page'] = $projects->perPage();
        $data['paging']['has_more'] = true;
        if($projects->currentPage() >= $projects->lastPage()){
            $data['paging']['has_more'] = false;
        }
        return response()->json($data);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'client_name' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'error' => $validator->errors(),
                'message' => trans('message.MESSAGE_FAILED'),
                'data' => null
            ),401);
        }

       $project = new Project;
       $project->name = $request->input('name');
       $project->client_name = $request->input('client_name');
       $project->description = $request->input('description');
       $project->project_detail = base64_encode($request->input('project_detail'));
       $project->creator_id = $request->auth->id;
       $project->status = 1;
       $project->save();

       //$projectObj = Project::select(DB::raw("MD5(id) as projectID, name, client_name, description, FROM_BASE64(project_detail) as project_detail, DATE_FORMAT(created_at, '%b %d, %Y') as created,created_at, DATE_FORMAT(updated_at, '%b %d, %Y') as updated, updated_at"));
       $projectObj = Project::select(DB::raw("MD5(id) as projectID, name, client_name, description, FROM_BASE64(project_detail) as project_detail, DATE_FORMAT(created_at, '%b %d, %Y') as created, created_at, DATE_FORMAT(updated_at, '%b %d, %Y') as updated, updated_at"), DB::raw("(SELECT SUM(project_status.tracking_hours) FROM project_status WHERE project_status.project_id = projects.id GROUP BY project_status.project_id) as hours"));
        $project = $projectObj->where('id', $project->id)->first();
        $data['error'] = false;
        $data['message'] = trans('message.MESSAGE_SUCCESS');
        $data['data'] = $project;
        return response()->json($data);
    }
    
    public function update(Request $request, $id) {
        $project = Project::where(DB::raw('MD5(id)'), $id)
            ->update(['project_detail' => base64_encode($request->input('project_detail'))]);
        
        //$projectObj = Project::select(DB::raw("MD5(id) as projectID, name, client_name,description, FROM_BASE64(project_detail) as project_detail, DATE_FORMAT(created_at, '%b %d, %Y') as created,created_at, DATE_FORMAT(updated_at, '%b %d, %Y') as updated, updated_at"));
        $projectObj = Project::select(DB::raw("MD5(id) as projectID, name, client_name, description, FROM_BASE64(project_detail) as project_detail, DATE_FORMAT(created_at, '%b %d, %Y') as created, created_at, DATE_FORMAT(updated_at, '%b %d, %Y') as updated, updated_at"), DB::raw("(SELECT SUM(project_status.tracking_hours) FROM project_status WHERE project_status.project_id = projects.id GROUP BY project_status.project_id) as hours"));
        $project = $projectObj->where(DB::raw('MD5(id)'), $id)->first();
        $data['error'] = false;
        $data['message'] = trans('message.MESSAGE_SUCCESS');
        $data['data'] = $project;
        return response()->json($data);
    }   
}
