<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\Project;
use DB, Validator, Auth;

class StatusController extends Controller {
    
    public function __construct() {
        
    }

    public function index(Request $request, $project_id) {
        $statuObj = Status::select(DB::raw("MD5(id) as status_id, MD5(project_id) as project_id, title, FROM_BASE64(detail) as detail, tracking_hours, DATE_FORMAT(created_at, '%b %d, %Y') as created, created_at, DATE_FORMAT(updated_at, '%b %d, %Y') as updated, updated_at"));
        $statuObj->where(DB::raw('MD5(project_id)'), $project_id);
        //$statuObj->where("project_id", "=", 5);
        if($request->input('q')!==""){
            echo $request->input('q');
            $statuObj->where('title','like', $request->input('q').'%');
            $statuObj->orWhere('detail','like',$request->input('q').'%');
        }
        //$statuObj->where('status','=',1);
        
        $status = $statuObj->paginate(12);

        $data['error'] = false;
		$data['message'] = trans('message.MESSAGE_SUCCESS');
		$data['data'] = $status->items();
        $data['paging']['total'] = $status->total();
        $data['paging']['prev'] = ($status->currentPage()>1)?($status->currentPage() - 1):null;
		$data['paging']['current'] = $status->currentPage();
        $data['paging']['next'] = ($status->currentPage()<$status->lastPage())?($status->currentPage() + 1):null;
        $data['paging']['per_page'] = $status->perPage();
		$data['paging']['has_more'] = true;
		if($status->currentPage() >= $status->lastPage()){
			$data['paging']['has_more'] = false;
        }
        return response()->json($data);
    }

    public function store(Request $request, $project_id) {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'tracking_hours' => 'required',
            'detail' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'error' => $validator->errors(),
                'message' => trans('message.MESSAGE_FAILED'),
                'data' => null
            ),401);
        }

        $project = Project::where(DB::raw('MD5(id)'), $project_id)->first();
        
        $status = new Status;
        $status->title = $request->input('title');
        $status->tracking_hours = $request->input('tracking_hours');
        $status->detail = base64_encode($request->input('detail'));
        $status->project_id = $project->id;
        $status->save();

        $statusObj = Status::select(DB::raw("MD5(id) as status_id, MD5(project_id) as project_id,title, FROM_BASE64(detail) as detail,tracking_hours, DATE_FORMAT(created_at, '%b %d, %Y') as created,created_at, DATE_FORMAT(updated_at, '%b %d, %Y') as updated, updated_at"));
        $status = $statusObj->where('id', $status->id)->first();
        $data['error'] = false;
        $data['message'] = trans('message.MESSAGE_SUCCESS');
        $data['data'] = $status;
        return response()->json($data);
    }

    public function update(Request $request,$project_id,$id) {
        $status = Status::where(DB::raw('md5("id")'), $id);
        $status->title = $request->input('name');
        $status->tracking_hours = $request->input('tracking_hours');
        $status->detail = base64_encode($request->input('detail'));
        $status->save();
        $statusObj = Status::select(DB::raw("MD5(id) as status_id,MD5(project_id) as project_id,title, FROM_BASE64(detail) as detail,tracking_hours, DATE_FORMAT(created_at, '%b %d, %Y') as created,created_at, DATE_FORMAT(updated_at, '%b %d, %Y') as updated, updated_at"));
        $statusObj->where('id', $status->id)->first();
        $data['error'] = false;
        $data['message'] = trans('message.MESSAGE_SUCCESS');
        $data['data'] = $statusObj;
        return response()->json($data);
    }
    
}
