<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    //$path = 'index.html';
    //return include(rtrim(app()->basePath('public/' . $path), '/'));
});
 /***** API  *****/
Route::group([], function()  use ($router) {

    $router->get('/', function () use ($router) {
        $data['error'] = true;
        $data['message'] = trans('message.WELCOME');
        return $data;
    });

    $router->post('auth/login','AuthController@authenticate');
    $router->group(['middleware' => 'jwt.auth'], function() use ($router) {
        /***** Auth *****/
        $router->get('profile','AuthController@profile');
        $router->post('profile','AuthController@profileUpdated');
        /***** Projects *****/
        $router->get('projects','ProjectController@index');
        $router->post('projects/create','ProjectController@store');
        $router->patch('projects/{id}/update','ProjectController@update');
        /***** Status Reports *****/
        $router->get('projects/{project_id}/reports','StatusController@index');
        $router->post('projects/{project_id}/reports/create','StatusController@store');
        $router->patch('projects/{project_id}/reports/{id}/update','StatusController@update');
        
   });

});