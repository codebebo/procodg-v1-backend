<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create( [
            'email' => 'admin@codebebo.com',
			'password' => Hash::make('password'),
			'name' => 'Admin',
			'role' => 'admin',
            'status' => '1',
            'token'=>''
        ] );

        User::create( [
            'email' => 'member@codebebo.com',
            'username' => 'member',
			'password' => Hash::make( 'password'),
			'name' => 'Member',
			'role' => 'member',
            'status' => '1',
            'token'=>''
        ] );
    }
}
