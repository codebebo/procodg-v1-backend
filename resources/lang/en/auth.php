<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'email_not'=>'Email does not exist.',
    'token'=>'Token not provided.',
    'token_expired'=>'Provided token is expired.',
    'token_decode'=>'An error while decoding token.',
    'account_blocked'=>'Your account is blocked!'

];
