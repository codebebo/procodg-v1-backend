<?php

return [
    "MESSAGE_SUCCESS" => "Success",
    "MESSAGE_FAILED" => "Failed",
    "MESSAGE_BAD_REQUEST" => "Bad Request",
    "MESSAGE_BAD_REQUEST_METHOD" => "Bad Request Method",
    "MESSAGE_CREATED" => "Created",
    "MESSAGE_UPDATED" => "Updated",
    "MESSAGE_DELETED" => "Deleted",
    "MESSAGE_PROJECT_ID_MISSING" => "Project Identifier is Missing",
    "VALIDATION_MESSAGE_INVALID" => "is not valid!",
    "VALIDATION_MESSAGE_EMPTY" => "is empty!",
    "VALIDATION_MESSAGE_MIN" => "lower than the minimum value!",
    "VALIDATION_MESSAGE_MAX" => "higher than the maximum value!",
    "VALIDATION_MESSAGE_NOT_EQUAL" => "is not equal",
    "VALIDATION_MESSAGE_EXCEEDS_LIMIT" => "exceeds the maximum size of",
    "VALIDATION_MESSAGE_NOT_EXT" => "it\'s not a",
    "VALIDATION_MESSAGE_DOES_NOT_MATCHED" => "info does not match",
    'WELCOME'=>'Welcome, You are connected!'

];